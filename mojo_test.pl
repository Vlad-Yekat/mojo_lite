use Mojolicious::Lite;
use Mojo::IOLoop;
use Mojo::Server::Daemon;

get '/' => {text => 'Hello Reg.ru!'};

get '/:name' => sub {
	shift->render('whois');
};


my $daemon = Mojo::Server::Daemon->new(
  app    => app,
  listen => ["http://*:30001"]
);

$daemon->run;

__DATA__
@@ whois.html.ep
Your name is <%= param('name') %>
